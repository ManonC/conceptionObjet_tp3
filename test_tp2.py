from boite import *

def test_creation_boite():
	b=Box()
	b.add("truc1")
	b.add("truc2")
	assert "truc1" in b
	assert "truc3" not in b
	b.sup("truc1")
	assert "truc1" not in b
	b.is_open()
	assert b.is_open()=="ouvert"
	b.close()
	assert b.is_open()=="ferme"
	assert b.action_look()=="la boite est fermee."
	b.open()
	b.add("truc4")
	assert b.action_look()=="la boite contient : truc2, truc4"

	t=Thing(3)
	assert t.volume()==3
	t.set_name("bidule")
	assert t.has_name("bidule")
	assert repr(t)=="< bidule >"

	assert b.capacity()==None
	b.set_capacity(5)
	assert b.capacity()==5

	assert b.has_room_for(t)==2
	t2=Thing(2)
	t2.set_name("toto")
	assert not t2.has_name("tata")
	assert repr(t2)=="< toto >"
	assert repr(t2)!="< tata >"
	assert b.has_room_for(t2)==0

	b.set_capacity(3)
	assert b.action_add(t)
	b.set_capacity(2)
	assert not b.action_add(t)
	b.close()
	assert not b.action_add(t2)

	assert b.find("bidule")