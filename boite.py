class Box:
	def __init__(self):
		self.__contents=[] # contents est privé
		self.etat="ouvert"
		self.capacite=None

	def add(self,truc):
		self.__contents.append(truc)

	def __contains__(self,machin):
		return machin in self.__contents

	def sup(self,truc):
		if truc in self.__contents:
			self.__contents.remove(truc)

	def is_open(self):
		return self.etat

	def close(self):
		self.etat="ferme"

	def open(self):
		self.etat="ouvert"

	def action_look(self):
		if self.is_open()=="ouvert":
			return "la boite contient : "+", ".join(self.__contents)
		else:
			return "la boite est fermee."

	def set_capacity(self,cap):
		self.capacite=cap

	def capacity(self):
		return self.capacite

	def has_room_for(self,t):
		if self.capacity()>=t.volume():
			self.capacite=self.capacity()-t.volume()
			return self.capacite
		else:
			return None

	def action_add(self,t):
		if self.has_room_for(t)!=None and self.is_open()=="ouvert":
			self.__contents.append(t)
			return True
		else:
			return False

	def find(self,nom):
		if t.self.nom==nom:
			return (t.__repr__, self.action_look())
		else:
			return None



class Thing:
	def __init__(self,poids):
		self.poids=poids
		self.nom=""

	def volume(self):
		return self.poids

	def set_name(self,nom):
		self.nom=nom

	def __repr__(self):
		return "< %s >" % (self.nom)

	def has_name(self,nom):
		if self.nom==nom:
			return True
		else:
			return False

	#je m'ennuie en cours 
	# coucou !!
	# je suis enchantée de faire votre connaissance :)
